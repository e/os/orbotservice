# Orbotservice

Orbotservice is an extract of the orbotservice module of [orbot sources](https://github.com/guardianproject/orbot), with a few patches to be used in Privacy Central.

This extract use the version of Orbot actually published on the [Google Play Store](https://play.google.com/store/apps/details?id=org.torproject.android), which garantee that the version is widely tested by users.

## How to upgrade orbotservice version.

We will take an example of migration from v16.4.1-RC-2-tor.0.4.4.6 , to an hypothetical v20.

1. Create a new branch `migration_V20` from tag v16.4.1-RC-2-tor.0.4.4.6
2. Get the zip of the sources of the v20 on orbot repository : https://github.com/guardianproject/orbot
3. Extract the orbotservice directory of the v20 sources to the migration_v20 workspace
4. Check and commit the changes.
5. Create the tag **v20**
6. Rebase main branch from tag v16.4.1-RC-2-tor.0.4.4.6 onto tag v20.
7. Fix conflicts and feature of each rebased commits.
8. Should be good !
