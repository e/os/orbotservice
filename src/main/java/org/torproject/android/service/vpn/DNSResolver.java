package org.torproject.android.service.vpn;

import android.util.Log;

import org.pcap4j.packet.DnsPacket;
import org.pcap4j.packet.IllegalRawDataException;
import org.torproject.android.service.OrbotService;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class DNSResolver {

    private final int mPort;
    private InetAddress mLocalhost = null;

    public DNSResolver(int localPort) {
        mPort = localPort;
    }

    public DnsPacket processDNS(DnsPacket dnsRequest) throws IOException {

        if (mLocalhost == null) mLocalhost = InetAddress.getLocalHost();

        if (OrbotService.shouldBlock != null) {
            DnsPacket blockedResponse = OrbotService.shouldBlock.apply(dnsRequest);
            if (blockedResponse != null) {
                return blockedResponse;
            }
        }

        byte[] payload = dnsRequest.getRawData();
        DatagramPacket packet = new DatagramPacket(
                payload, payload.length, mLocalhost, mPort
        );
        DatagramSocket datagramSocket = new DatagramSocket();
        datagramSocket.send(packet);

        // Await response from DNS server
        byte[] buf = new byte[1024];
        packet = new DatagramPacket(buf, buf.length);
        datagramSocket.receive(packet);

        datagramSocket.close();

        byte[] dnsResp = packet.getData();
        DnsPacket dnsResponse = null;
        if (dnsResp != null) {
            try {
                dnsResponse = DnsPacket.newPacket(dnsResp, 0, dnsResp.length);
            } catch (IllegalRawDataException e) {
                Log.e("DNSResolver", "Can't parse DNS response", e);
            }
        }
        return dnsResponse;
    }
}
